
import java.util.Date;

public class Block {

	private String hash;
	private String previousHash;
	private String data; //our data will be a simple message.
	private long timeStamp; //as number of milliseconds since 1/1/1970.
	private int tries;

	
	public int getTries() {
		return tries;
	}

	public void setTries(int tries) {
		this.tries = tries;
	}

	public String getPreviousHash() {
		return previousHash;
	}

	public void setPreviousHash(String previousHash) {
		this.previousHash = previousHash;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}


	public Block(String data,String previousHash ) {
		this.data = data;
		this.previousHash = previousHash;
		this.timeStamp = new Date().getTime();
		this.hash = getHash(); 
	}
	
	
	
	public String getHash() {
		String hash = Hash.createHash( 
				previousHash +
				Long.toString(timeStamp) +
				data 
				);
		return hash;
	}
	
	
	public void mineBlock(int difficulty) {
		String target = new String(new char[difficulty]).replace('\0', '0');  
		while(!hash.substring( 0, difficulty).equals(target)) {
			tries ++;
			hash = getHash();
		}
		System.out.println("Nonesense coin Mined!!! : " + hash);
	}
}
