import java.util.ArrayList;
import java.util.Scanner;


public class BlockChainMain {

	public static ArrayList<Block> blockchain = new ArrayList<Block>();
	public static int difficulty = 0; //difficulty level for mining blocks
	
	public static void main(String[] args) {
		
		System.out.println("Enter Data for first block:");
        Scanner scanner = new Scanner(System.in); //create instance of scanner class
		String userInput = scanner.nextLine(); //gets user input
		
		//add our blocks to the blockchain ArrayList:
		blockchain.add(new Block(userInput, "0"));
		System.out.println("\nTrying to Mine block 1... ");
		blockchain.get(0).mineBlock(difficulty);
		
		blockchain.add(new Block("Second block",blockchain.get(blockchain.size()-1).getHash()));
		System.out.println("\nTrying to Mine block 2... ");
		blockchain.get(1).mineBlock(difficulty);
		
		blockchain.add(new Block("Third block",blockchain.get(blockchain.size()-1).getHash()));
		System.out.println("\nTrying to Mine block 3... ");
		blockchain.get(2).mineBlock(difficulty);	
		
		blockchain.add(new Block("Fourth block",blockchain.get(blockchain.size()-1).getHash()));
		System.out.println("\nTrying to Mine block 4... ");
		blockchain.get(3).mineBlock(difficulty);
		
		System.out.println();
		System.out.println(isChainValid());
		System.out.println();
				
				for(Block block: blockchain)
				{
					System.out.println("Hash:"+block.getHash());
					System.out.println("Previous Hash:"+block.getPreviousHash());
					System.out.println("Data:"+block.getData());
					System.out.println();
				}

	}
	
	
	public static String isChainValid() {
		Block currentBlock; 
		Block previousBlock;
		String hashTarget = new String(new char[difficulty]).replace('\0', '0');
		
		//loop through blockchain to check hashes:
		for(int i=1; i < blockchain.size(); i++) {
			currentBlock = blockchain.get(i);
			previousBlock = blockchain.get(i-1);
			//compare registered hash and calculated hash:
			if(!currentBlock.getHash().equals(currentBlock.getHash()) ){
				System.out.println("Current Hashes not equal");			
				return "Current Hashes not equal";
			}
			//compare previous hash and registered previous hash
			if(!previousBlock.getHash().equals(currentBlock.getPreviousHash()) ) {
				System.out.println("Previous Hashes not equal");
				return "Previous Hashes not equal";
			}
			//check if hash is solved
			if(!currentBlock.getHash().substring( 0, difficulty).equals(hashTarget)) {
				System.out.println("This block hasn't been mined");
				return "This block hasn't been mined";
			}
		}
		return "Blockchain is Valid: YOU GET A NONSENSE COIN!";
	}

}
